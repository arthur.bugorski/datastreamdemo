package snsdemo;

import com.amazonaws.auth.policy.Policy;
import com.amazonaws.auth.policy.Principal;
import com.amazonaws.auth.policy.Resource;
import com.amazonaws.auth.policy.Statement;
import com.amazonaws.auth.policy.actions.SQSActions;
import com.amazonaws.auth.policy.conditions.ArnCondition;
import com.amazonaws.auth.policy.conditions.ConditionFactory;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.*;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.*;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutorService;

@Service
public class DataStreamServiceImpl implements DataStreamService {

    private final AmazonSNSClient amazonSNSClient;
    private final AmazonSQSClient amazonSQSClient;
    private final UserQueueDataRepositoryImpl userQueueDataRepository;
    private final HeartbeatRepositoryImpl heartbeatRepository;
    private final ExecutorService executorService;

    private static final JsonNodeFactory JSON_NODE_FACTORY = new JsonNodeFactory(false);

    private static final String TOPIC_ARN = "arn:aws:sns:ca-central-1:518380161565:RRTestSns";

    private final Duration maxTimeAway;

    @Autowired
    public DataStreamServiceImpl(
            @Value("${datastream.maxtimeawaymin}")
            final Integer maxTimeAwayMinutes,
            final AmazonSNSClient amazonSNSClient,
            final AmazonSQSClient amazonSQSClient,
            final UserQueueDataRepositoryImpl userQueueDataRepository,
            final HeartbeatRepositoryImpl heartbeatRepository,
            final ExecutorService executorService
    ){
        this.amazonSNSClient = amazonSNSClient;
        this.amazonSQSClient = amazonSQSClient;
        this.userQueueDataRepository = userQueueDataRepository;
        this.heartbeatRepository = heartbeatRepository;
        this.executorService = executorService;
        this.maxTimeAway = Duration.ofMinutes(maxTimeAwayMinutes);
    }

    private static String getTopicForUser(final String username){
        /*
         * This is hardcoded for now, but you can imagine that this could be tied to the users market etc.
         */
        return TOPIC_ARN;
    }


    private UserSubscriptionQueue getOrCreateQueueForUser( final String username ){

        UserSubscriptionQueue userSubscriptionQueue = null;
        do {
            final Optional<UserSubscriptionQueue> oUserQueueUrl = userQueueDataRepository.getQueueForUser(username);

            if( oUserQueueUrl.isPresent() ) {
                userSubscriptionQueue = oUserQueueUrl.get();

            }else{
                // if there is no data for the user, then we need to create a connection... but be sure we are the only one
                // doing so to prevent a resource leak, so we have to acquire a lock for the user

                if(  userQueueDataRepository.getLockForUserQueueCreation( username )  ){

                    //since we have the lock we can create resources for the user
                    final URL queueUrl = createQueueForUser(username);
                    openQueueToTopic( username, queueUrl );
                    final UserSubscriptionQueue usq = new UserSubscriptionQueue();
                    usq.queue = queueUrl;

                    executorService.execute(  ()  ->  userQueueDataRepository.setQueueForUsername( username, usq )  );


                }else{
                    // we *didn't* get the lock, someone else is creating the credentials... wait and ask for them again
                    try{
                        Thread.sleep(30 * 1000L);//TODO make variable
                    }catch ( InterruptedException e ){
                        //TODO log this, maybe count times we have been waiting, maybe max attempt?
                    }
                }
            }


        }while( userSubscriptionQueue != null );

        return userSubscriptionQueue;
    }

    public void keepAlive( final String username ){
        heartbeatRepository.updateHeartbeat(username);
    }

    @Scheduled( fixedDelayString = "#{datastream.deadcheckfrequencymin} * 60 * 1000" )
    public void closeDeadStreams(){
        heartbeatRepository.listColdHeartbeatUsers().forEach( heartbeatRepository::stopTrackingHeartbeatsForUsername );
    }

    private void openQueueToTopic( final String username, final URL queueUrl){
        // https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/java-dg-access-control.html#sqs-example
        final Policy allowTopicToWritePolicy = new Policy().withStatements(
                new Statement(Statement.Effect.Allow)
                        .withPrincipals(Principal.AllUsers)
                        .withActions(SQSActions.SendMessage)
                        .withResources(
                                new Resource( toArn(queueUrl) )
                        )
                        .withConditions(
                                new ArnCondition(
                                        ArnCondition.ArnComparisonType.ArnEquals,
                                        ConditionFactory.SOURCE_ARN_CONDITION_KEY,
                                        getTopicForUser(username)
                                )
                        )
        );
        final String allowTopicToWritePolicyJson = allowTopicToWritePolicy.toJson();

        amazonSQSClient.setQueueAttributes(
                new SetQueueAttributesRequest()
                        .withQueueUrl(queueUrl.toExternalForm())
                        .addAttributesEntry( QueueAttributeName.MessageRetentionPeriod.toString(), Long.toString( maxTimeAway.toSeconds() ) )
                        .addAttributesEntry( QueueAttributeName.Policy.toString(), allowTopicToWritePolicyJson )
        );
    }

    private URL createQueueForUser(String queuelessUser) {
        final UUID userUuid = UUID.randomUUID();
        try {
            // by having a UUID in the name we have uniqueness and prevent collisions, also it increases
            // resilience to hackability since the name aren't sequential nor guessable. By including the
            // username we have some semblance of traceability.
            final String queueName = queuelessUser + "_" + userUuid;

            //TODO set the message retention policy to 15 minutes or something
            final CreateQueueResult createQueueResult = amazonSQSClient.createQueue(
                    new CreateQueueRequest()
                            .withQueueName(queueName)
            );
            final URL queueUrl = new URL(createQueueResult.getQueueUrl());
            return queueUrl;

        } catch ( QueueNameExistsException | QueueDeletedRecentlyException | MalformedURLException e) {
            //this will never happen because of UUID's uniqueness
            throw new RuntimeException( "Failed to create queue for user: " + queuelessUser, e );
        }
    }

    public URL createStreamForUser( final String username, List<String> initialTrackedShipments ){
        /*
         * TODO need some sort of locking
         *
         * Something like HGET the URL, on failure to find, SADDing the username to the queue. If result is NIL
         * using redis to SADD the username to a queue creation queue, if the result is "1", create the queue, HPUT the
         * queue URL. If there was an error on queue creation, SREM the name.
         *
         * Otherwise, if you did not get a "1" when SADDing, just sleep for a fem minutes and try again.
         *
         * https://redis.io/commands/sadd
         */
        final UserSubscriptionQueue queueForUser = getOrCreateQueueForUser(username);


        if( queueForUser.subscriptionArn == null ) {
            final SubscribeResult subscribeRequest = amazonSNSClient.subscribe(
                    new SubscribeRequest()
                            .withTopicArn(getTopicForUser(username))
                            .withProtocol("sqs")
                            .withEndpoint(toArn(queueForUser.queue))
            );
            queueForUser.subscriptionArn = subscribeRequest.getSubscriptionArn();
        }

        setUsersTrackedShipments( username, initialTrackedShipments );

        return queueForUser.queue;
    }


    public void setUsersTrackedShipments( final String username, List<String> shipments ){
        final UserSubscriptionQueue queueForUser = userQueueDataRepository.getQueueForUser(username)
                .orElseThrow(  ()  ->  new IllegalStateException( "No queue for user: " + username )  )
            ;

        // Initialize the example filter policy class.
        final SNSMessageFilterPolicy fp = new SNSMessageFilterPolicy();
        fp.addAttribute(
                "shipments",
                shipments
            );

        final SetSubscriptionAttributesRequest request =
                new SetSubscriptionAttributesRequest(queueForUser.subscriptionArn,
                        "FilterPolicy", fp.formatFilterPolicy());
        amazonSNSClient.setSubscriptionAttributes(request);
    }

    private static String toArn( final URL aUrl ){
        // https://sqs.ca-central-1.amazonaws.com/518380161565/RRTestSqs
        // arn:aws:sqs:ca-central-1:518380161565:RRTestSqs

        final String [] subdomains = aUrl.getHost().split("\\.");
        final String [] folders = aUrl.getPath().split("/");
        return "arn:aws:" + subdomains[0] + ":" + subdomains[1] + ":" + folders[1] + ":" + folders[2];
    }

    public void deleteUserSubscription(final String username ){
        executorService.execute( () -> {
                final UserSubscriptionQueue queueForUser = userQueueDataRepository.getQueueForUser(username)
                        .orElseThrow(() -> new IllegalStateException("No queue for user: " + username));

                executorService.execute(() -> {
                    amazonSNSClient.unsubscribe(queueForUser.subscriptionArn);
                    amazonSQSClient.deleteQueue(
                            new DeleteQueueRequest()
                                    .withQueueUrl(queueForUser.queue.toExternalForm())
                    );
                });
                userQueueDataRepository.removeUserQueue(username);
                heartbeatRepository.stopTrackingHeartbeatsForUsername(username);
            });
    }


    public void updateStreams( final List<String> shipmentIds, final String message ){
        final ArrayNode shipmentsAttributeJson = JSON_NODE_FACTORY.arrayNode(shipmentIds.size());
        shipmentIds.forEach(shipmentsAttributeJson::add);
        final String shipmentsAttributeValue = shipmentsAttributeJson.toString();

        amazonSNSClient.publish(
                new PublishRequest()
                        //TODO this should be a lookup per driver
                        .withTopicArn(TOPIC_ARN)
                        .withMessage(message)
                        .withMessageAttributes(
                                ImmutableMap.of(
                                        "shipments",
                                        new MessageAttributeValue()
                                                .withDataType("String.Array")
                                                .withStringValue( shipmentsAttributeValue )
                                    )
                            )
            );
    }


}
