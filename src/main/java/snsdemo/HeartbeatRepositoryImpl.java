package snsdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Repository;

import java.time.Duration;
import java.time.Instant;
import java.util.Set;
import java.util.concurrent.ExecutorService;

@Repository
public class HeartbeatRepositoryImpl implements HeartbeatRepository {

    private static final String HEARTBEAT_SORTED_SET = "users_by_last_heartbeat_time";

    private final ZSetOperations<String,String> sortedSetOperations;
    private final ExecutorService executorService;
    private final Duration minutesWithoutHeartbeatToConsiderDead;


    @Autowired
    public HeartbeatRepositoryImpl(
            final RedisOperations<String, String> redisOperations,
            final ExecutorService executorService,

            @Value("${datastream.maxtimeawaymin}")
            final Integer minutesWithoutHeartbeatToConsiderDead
    ){
        this.sortedSetOperations = redisOperations.opsForZSet();
        this.executorService = executorService;
        this.minutesWithoutHeartbeatToConsiderDead = Duration.ofMinutes( minutesWithoutHeartbeatToConsiderDead );
    }


    public void updateHeartbeat( String username ){
        executorService.execute( () -> sortedSetOperations.add( HEARTBEAT_SORTED_SET, username, System.currentTimeMillis() ) );
    }


    public void stopTrackingHeartbeatsForUsername( final String username ){
        executorService.execute( () -> sortedSetOperations.remove( username ) );
    }

    //TODO this would be nice to be a reactive stream so that we can use limit and not black everything
    public Set<String> listColdHeartbeatUsers(){
        final Instant timeAfterWhichToConsiderDead = Instant.now().minus( minutesWithoutHeartbeatToConsiderDead );
        return sortedSetOperations.rangeByScore( HEARTBEAT_SORTED_SET, 0, timeAfterWhichToConsiderDead.toEpochMilli() );
    }

}
