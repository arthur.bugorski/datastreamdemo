//package snsdemo;
//
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.node.ArrayNode;
//import com.fasterxml.jackson.databind.node.JsonNodeFactory;
//import com.fasterxml.jackson.databind.node.ObjectNode;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.RedisOperations;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.UUID;
//import java.util.concurrent.ExecutorService;
//
//@RestController
//public class SnsController {
//
//    private static final JsonNodeFactory JSON_NODE_FACTORY = new JsonNodeFactory(false);
//
//    @Autowired
//    private ExecutorService executorService;
//
//    @Autowired
//    private DataStreamServiceImpl dataStreamService;
//
//    @PostMapping(
//        value = "/stream/{username}",
//        consumes = MediaType.APPLICATION_JSON_VALUE,
//        produces = MediaType.APPLICATION_JSON_VALUE
//    )
//    public ResponseEntity<JsonNode> createStreamForUser(
//            @PathVariable("username") final String username,
//            @RequestBody StreamRequest streamRequest
//    ){
//        final String queueUrlValue = dataStreamService.createStreamForUser(username, streamRequest.getShipments()).toExternalForm();
//
//        final ObjectNode responseRoot = JSON_NODE_FACTORY.objectNode();
//        responseRoot.put("username",username);
//        ArrayNode shipmentsNode = JSON_NODE_FACTORY.arrayNode();
//        streamRequest.getShipments().forEach(shipmentsNode::add);
//        responseRoot.set("shipments",shipmentsNode);
//        responseRoot.put("stream",queueUrlValue);
//
//        return new ResponseEntity<>(responseRoot,HttpStatus.OK);
//    }
//
//
//    @PostMapping(
//            value = "/stream/{username}/shipments",
//            consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE
//    )
//    public String setShipmentsForUser(
//            @PathVariable("username") final String username,
//            @RequestBody StreamRequest streamRequest
//    ){
//        dataStreamService.setUsersTrackedShipments( username, streamRequest.getShipments() );
//        return "{\"shipments\":\"" + streamRequest.getShipments() +"\"}";
//    }
//
//
//    @GetMapping("/stream/{username}")
//    public ResponseEntity<JsonNode> heartbeat( @PathVariable("username") final String username ){
//        executorService.execute( () -> dataStreamService.keepAlive(username) );
//        return new ResponseEntity<>(HttpStatus.OK);
//    }
//
//
//    @DeleteMapping(
//            value = "/stream/{username}",
//            consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE
//    )
//    public ResponseEntity<JsonNode> deleteStreamForUser( @PathVariable("username") final String username ){
//        dataStreamService.deleteUserSubscription(username);
//        return new ResponseEntity<>(HttpStatus.OK);
//    }
//
//
//    @PostMapping(
//            value = "/message",
//            consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE
//    )
//    public ResponseEntity<JsonNode> updatePosition( final DeliveryPositionUpdate deliveryPositionUpdate ){
//        executorService.execute( () -> {
//                final ObjectNode messageJson = JSON_NODE_FACTORY.objectNode();
//                messageJson.put("latitude", deliveryPositionUpdate.getLatitude() );
//                messageJson.put("longitude", deliveryPositionUpdate.getLongitude());
//                final String messageValue = messageJson.toString();
//
//                dataStreamService.updateStreams(deliveryPositionUpdate.getShipments(), messageValue );
//            });
//
//        return new ResponseEntity<>(HttpStatus.OK);
//    }
//
//    @Autowired(required = true)
//    RedisOperations<String,String> redisOperations;
//
//
//
//    @GetMapping("/hw")
//    public String aoeu(){
//        redisOperations.opsForValue().set( "key", "value" + UUID.randomUUID() );
//
//        return "hello world!"
//                + redisOperations + "\n"
//                + redisOperations.opsForValue().get( "key" )
//                ;
//    }
//
//}
