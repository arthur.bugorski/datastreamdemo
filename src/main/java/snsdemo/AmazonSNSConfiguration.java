package snsdemo;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class AmazonSNSConfiguration {



    @Bean
    public AWSCredentialsProvider awsCredentialsProvider(
        @Value("${cloud.aws.credentials.access-key}")
        final String accessKey,

        @Value("${cloud.aws.credentials.secret-key}")
        final String secretKey
    ){
        return new AWSStaticCredentialsProvider(
                new BasicAWSCredentials(
                        accessKey,
                        secretKey
                )
        );
    }


    @Primary
    @Bean
    public AmazonSNSClient amazonSNSClient( AWSCredentialsProvider awsCredentialsProvider ) {
        return (AmazonSNSClient) AmazonSNSClientBuilder
                .standard()
                .withRegion(Regions.CA_CENTRAL_1)
                //probably don't need the following
                .withCredentials( awsCredentialsProvider )
                .build();
    }

    @Bean
    public AmazonSQSClient amazonSQSClient( AWSCredentialsProvider awsCredentialsProvider ) {
        return (AmazonSQSClient) AmazonSQSClientBuilder
                .standard()
                .withRegion(Regions.CA_CENTRAL_1)
                //probably don't need the following
                .withCredentials( awsCredentialsProvider )
                .build();
    }

    @Bean
    public ExecutorService executorService(){
        return Executors.newCachedThreadPool();
    }

}
