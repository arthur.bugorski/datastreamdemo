package snsdemo;

import java.util.List;

public class StreamRequest {
    private List<String> shipments;

    public List<String> getShipments() {
        return shipments;
    }

    public void setShipments(List<String> shipments) {
        this.shipments = shipments;
    }
}
