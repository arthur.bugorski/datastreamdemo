package snsdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.stereotype.Repository;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutorService;


@Repository
public class UserQueueDataRepositoryImpl implements UserQueueDataRepository {

    private static final String USER_QUEUE_URL_BY_USERNAME = "queue_subscription_pair_by_username";
    private static final String USER_QUEUE_CREATION_SET = "user_queue_creation_mutex";
    private static final Long SADD_ADDED = 1L;
    private static final String QUEUE_SUBSCRIPTION_SEPARATOR = "\\|";

    private final SetOperations<String,String> setOperations;
    private final HashOperations<String,Object,Object> hashOperations;
    private final ExecutorService executorService;

    @Autowired
    public UserQueueDataRepositoryImpl(final RedisOperations<String, String> redisOperation, ExecutorService executorService) {
        this.setOperations = redisOperation.opsForSet();
        this.hashOperations = redisOperation.opsForHash();
        this.executorService = executorService;
    }


    public Optional<UserSubscriptionQueue> getQueueForUser( final String username ) {
        final Optional<Object> oQueueUrlSubscriptionArnPair = Optional.ofNullable(  hashOperations.get( USER_QUEUE_URL_BY_USERNAME, username )  );

        if( oQueueUrlSubscriptionArnPair.isEmpty() ){
            return Optional.empty();
        }

        final String[] queueUrlSubscriptionArnPair = oQueueUrlSubscriptionArnPair.get().toString().split(QUEUE_SUBSCRIPTION_SEPARATOR);
        final UserSubscriptionQueue usq = new UserSubscriptionQueue();

        try{
            usq.queue = new URL( queueUrlSubscriptionArnPair[0] );

        }catch( MalformedURLException e ){
            throw new IllegalArgumentException();

        }
        usq.subscriptionArn = queueUrlSubscriptionArnPair[1];

        return Optional.of( usq );
    }


    public void setQueueForUsername( final String username, final UserSubscriptionQueue userSubscriptionQueue ){
        hashOperations.put(
                USER_QUEUE_URL_BY_USERNAME,
                username,
                userSubscriptionQueue.queue.toExternalForm() + QUEUE_SUBSCRIPTION_SEPARATOR + userSubscriptionQueue.subscriptionArn
            );
    }

    public boolean getLockForUserQueueCreation( final String username ){
        return Objects.equals(
                //this is the "lock"; only the first to add will get a response of 1, otherwise 0
                setOperations.add( USER_QUEUE_CREATION_SET, username ),
                SADD_ADDED
            );
    }

    public void removeUserQueue( final String username ){
        executorService.execute( ()-> {
                hashOperations.delete(USER_QUEUE_URL_BY_USERNAME, username);
                setOperations.remove(USER_QUEUE_CREATION_SET, username);
            });
    }

}
