package com.renorun.web.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.renorun.domain.DataStreamSubscriptionQueue;
import com.renorun.domain.Notification;
import com.renorun.service.DataStreamService;
import com.renorun.service.MessagePublishingService;
import com.renorun.service.UserDataStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

@RestController
public class DataStreamController {
    private static final Logger LOGGER = LoggerFactory.getLogger( DataStreamController.class );

    private static final JsonNodeFactory JSON_NODE_FACTORY = new JsonNodeFactory(false);

    @Autowired
    private ExecutorService executorService;

    @Autowired
    private DataStreamService dataStreamService;

    @Autowired
    private MessagePublishingService messagePublishingService;

    @PutMapping(
        value = "/stream/{userName}/{deviceId}",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<JsonNode> createStreamForUser(
            @PathVariable("userName") final String username,
            @PathVariable("deviceId") final String deviceId,
            @RequestBody Map< Object, List<String> > messageAttributeValuesByMessageAttributeName
    ){
        final DataStreamSubscriptionQueue dssq = dataStreamService.create(
                new UserDataStream( username, deviceId ),
                messageAttributeValuesByMessageAttributeName
        );

        final ObjectNode responseRoot = JSON_NODE_FACTORY.objectNode();
        responseRoot.put("username",username);
        final ObjectNode attributesNode = JSON_NODE_FACTORY.objectNode();
        messageAttributeValuesByMessageAttributeName.forEach( (attributeName,attributeValues) -> {
                final ArrayNode attributeValuesNode = JSON_NODE_FACTORY.arrayNode(attributeValues.size());
                attributeValues.forEach( attributeValuesNode::add );
                attributesNode.set( attributeName.toString(), attributeValuesNode );
            });
        responseRoot.set("attributes",attributesNode);

        final ObjectNode streamNode = JSON_NODE_FACTORY.objectNode();
        streamNode.put( "url"      , dssq.getQueue().toExternalForm() );
        streamNode.put( "username" , dssq.getAwsUser().getUserName()  );
        streamNode.put( "password" , dssq.getAwsUser().getPassword()  );
        responseRoot.set("stream", streamNode );

        return new ResponseEntity<>(responseRoot,HttpStatus.OK);
    }


    @PutMapping("/stream/{userName}/{deviceId}/heartbeat")
    public void heartbeat(
            @PathVariable("userName") final String username,
            @PathVariable("deviceId") final String deviceId
    ){
        executorService.execute( () -> {
            dataStreamService.keepAlive( new UserDataStream(username, deviceId) );
            LOGGER.debug( "Heartbeat for {]" );
        });
    }


    @DeleteMapping(
            value = "/stream/{userName}/{deviceId}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<JsonNode> deleteStreamForUser(
            @PathVariable("userName") final String username,
            @PathVariable("deviceId") final String deviceId
    ){
        dataStreamService.delete( new UserDataStream(username, deviceId) );
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @PostMapping(
            value = "/notify",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<JsonNode> notify( @RequestBody final Notification notification ){
        executorService.execute( () ->
                messagePublishingService.updateStreams(
                    notification.getMetadata(),
                    notification.getMessage()
                )
        );

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
