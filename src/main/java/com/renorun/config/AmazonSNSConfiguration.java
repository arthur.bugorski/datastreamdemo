package com.renorun.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.EnableScheduling;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.services.iam.IamClient;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
@EnableScheduling
public class AmazonSNSConfiguration {


    @Primary
    @Bean
    public AmazonSNSClient amazonSNSClient(
            @Value("${cloud.aws.credentials.access-key}")
            final String accessKey,

            @Value("${cloud.aws.credentials.secret-key}")
            final String secretKey
    ) {
        return
         (AmazonSNSClient) AmazonSNSClientBuilder
                .standard()
                .withRegion(Regions.CA_CENTRAL_1)
                //probably don't need the following
                .withCredentials(
                        new AWSStaticCredentialsProvider(
                                new BasicAWSCredentials(
                                        accessKey,
                                        secretKey
                                )
                        )
                )
                .build();
    }



    @Bean
    public AmazonSQSClient amazonSQSClient(
            @Value("${cloud.aws.credentials.access-key}")
            final String accessKey,

            @Value("${cloud.aws.credentials.secret-key}")
            final String secretKey
    ) {
        return (AmazonSQSClient) AmazonSQSClientBuilder
                .standard()
                .withRegion(Regions.CA_CENTRAL_1)
                //probably don't need the following
                .withCredentials(
                        new AWSStaticCredentialsProvider(
                                new BasicAWSCredentials(
                                        accessKey,
                                        secretKey
                                )
                        )
                )
                .build();
    }

//    @Bean
//    public com.amazonaws.auth.AWSCredentialsProvider AWSCredentialsProvider(
//        @Value("${cloud.aws.credentials.access-key}")
//        final String accessKey,
//
//        @Value("${cloud.aws.credentials.secret-key}")
//        final String secretKey
//    ){
//        return new AWSStaticCredentialsProvider(
//                new BasicAWSCredentials(
//                        accessKey,
//                        secretKey
//                )
//        );
//    }
//
//
//    @Primary
//    @Bean
//    public AmazonSNSClient amazonSNSClient( com.amazonaws.auth.AWSCredentialsProvider awsCredentialsProvider ) {
//        return (AmazonSNSClient) AmazonSNSClientBuilder
//                .standard()
//                .withRegion(Regions.CA_CENTRAL_1)
//                //probably don't need the following
//                .withCredentials( awsCredentialsProvider )
//                .build();
//    }
//
//
//    @Bean
//    public AmazonSQSClient amazonSQSClient( com.amazonaws.auth.AWSCredentialsProvider awsCredentialsProvider ) {
//        return (AmazonSQSClient) AmazonSQSClientBuilder
//                .standard()
//                .withRegion(Regions.CA_CENTRAL_1)
//                //probably don't need the following
//                .withCredentials( awsCredentialsProvider )
//                .build();
//    }


    @Bean
    public software.amazon.awssdk.auth.credentials.AwsCredentialsProvider awsCredentialsProvider(
        @Value("${cloud.aws.credentials.access-key}")
        final String accessKey,

        @Value("${cloud.aws.credentials.secret-key}")
        final String secretKey
    ){
        return StaticCredentialsProvider.create(
                AwsBasicCredentials.create( accessKey, secretKey )
        );
    }


    @Bean
    public IamClient iamClient( software.amazon.awssdk.auth.credentials.AwsCredentialsProvider awsCredentialsProvider ) {
        return IamClient.builder()
                .credentialsProvider( awsCredentialsProvider )
                .build()
                ;

    }

    @Bean
    public ExecutorService executorService(){
        return Executors.newCachedThreadPool();
    }

}
