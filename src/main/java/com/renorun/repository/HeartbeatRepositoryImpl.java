package com.renorun.repository;

import com.renorun.service.DataStream;
import com.renorun.service.GenericDataStream;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Repository;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

@Repository
public class HeartbeatRepositoryImpl implements HeartbeatRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger( HeartbeatRepositoryImpl.class );

    private final ZSetOperations<String,String> sortedSetOperations;
    private final ExecutorService executorService;
    private final String heartBeatSortedSetKeyName;
    private final Duration minutesWithoutHeartbeatToConsiderDead;


    @Autowired
    public HeartbeatRepositoryImpl(
            final RedisOperations<String, String> redisOperations,

            @Value( "${datastream.redis.users-by-last-heartbeat-time}" )
            final String heartBeatSortedSetKeyName,

            @Value( "${datastream.max-time-away-min}" )
            final Integer minutesWithoutHeartbeatToConsiderDead,

            final ExecutorService executorService
        ){
        this.sortedSetOperations = redisOperations.opsForZSet();
        this.executorService = executorService;
        this.heartBeatSortedSetKeyName = heartBeatSortedSetKeyName;
        this.minutesWithoutHeartbeatToConsiderDead = Duration.ofMinutes( minutesWithoutHeartbeatToConsiderDead );
    }


    /** {@inheritDoc} */
    @Override
    public void startTrackingHeartbeatsForStream( final DataStream stream ){
        /*
         * This is not needed for this implementation since Redis will create the key if it is missing, bet adding a
         * separate method allows for a different implementation.
         */
        updateHeartbeat( stream );
    }


    /** {@inheritDoc} */
    @Override
    public void updateHeartbeat( final DataStream stream ){
        executorService.execute( () -> sortedSetOperations.add( heartBeatSortedSetKeyName, stream.toIdentifier(), getNow().toEpochMilli() ) );
    }


    /**
     * This method only exists so that it can be overridden in unit tests since accessesing the system clock is not
     * deterministic.
     */
    protected Instant getNow(){
        return Instant.now();
    }


    /** {@inheritDoc} */
    @Override
    public void stopTrackingHeartbeatsForStream( final DataStream stream ){
        executorService.execute( () -> sortedSetOperations.remove( heartBeatSortedSetKeyName, stream.toIdentifier()) );
    }


    /** {@inheritDoc} */
    //TODO this would be nice to be a reactive stream so that we can use limit and not black everything
    @Override
    public Set<DataStream> listColdHeartbeatStreams(){
        final Instant timeAfterWhichToConsiderDead = getNow().minus( minutesWithoutHeartbeatToConsiderDead );
        final long deadlineInMillisSinceEpoch = timeAfterWhichToConsiderDead.toEpochMilli();
        LOGGER.debug( "looking for streams from whom we have not heard since: {}", deadlineInMillisSinceEpoch );
        return ObjectUtils.defaultIfNull(
                    sortedSetOperations.rangeByScore( heartBeatSortedSetKeyName, 0, deadlineInMillisSinceEpoch ),
                    Collections.<String>emptySet()
                )
                .stream()
                .map( GenericDataStream::new )
                .collect( Collectors.toSet() )
                ;
    }

}
