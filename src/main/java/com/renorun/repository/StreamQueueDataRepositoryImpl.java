package com.renorun.repository;

import com.renorun.domain.DataStreamSubscriptionQueue;
import com.renorun.service.DataStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.stereotype.Repository;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutorService;


@Repository
public class StreamQueueDataRepositoryImpl implements StreamQueueDataRepository {

    private static final Long SADD_ADDED = 1L;

    private final SetOperations<String,String> setOperations;
    private final HashOperations<String,Object,Object> hashOperations;
    private final ExecutorService executorService;

    private final String streamQueueUrlByIdentifier;
    private final String streamQueueCreationMutexSet;


    @Autowired
    public StreamQueueDataRepositoryImpl(
            final RedisOperations<String, String> redisOperation,

            @Value( "${datastream.redis.queue-subscription}" )
            final String streamQueueUrlByStreamIdentifier,

            @Value( "${datastream.redis.queue-mutex}" )
            final String streamQueueCreationMutexSet,

            final ExecutorService executorService
    ){
        this.setOperations = redisOperation.opsForSet();
        this.hashOperations = redisOperation.opsForHash();
        this.executorService = executorService;

        this.streamQueueUrlByIdentifier = streamQueueUrlByStreamIdentifier;
        this.streamQueueCreationMutexSet = streamQueueCreationMutexSet;
    }


    /** {@inheritDoc} */
    @Override
    public Optional< DataStreamSubscriptionQueue > getQueueForStream( final DataStream stream ){
        // this field get populated by {@link #setQueueForStream}
        final Optional<Object> oQueueUrlSubscriptionArnPair = Optional.ofNullable(  hashOperations.get( streamQueueUrlByIdentifier, stream.toIdentifier() )  );

        if( oQueueUrlSubscriptionArnPair.isEmpty() ){
            return Optional.empty();
        }

        return Optional.of( DataStreamSubscriptionQueue.deserialise( oQueueUrlSubscriptionArnPair.get().toString() ) );
    }


    /** {@inheritDoc} */
    @Override
    public void setQueueForStream( final DataStream stream, final DataStreamSubscriptionQueue dataStreamSubscriptionQueue ){
        executorService.execute( () ->
                hashOperations.put(
                        streamQueueUrlByIdentifier,
                        stream.toIdentifier(),
                        DataStreamSubscriptionQueue.serialise( dataStreamSubscriptionQueue )
                )
            );
    }


    /** {@inheritDoc} */
    @Override
    public boolean acquireLockForStreamQueueCreation( final DataStream stream ){
        return Objects.equals(
                //this is the "lock"; only the first to add will get a response of 1, otherwise 0
                setOperations.add( streamQueueCreationMutexSet, stream.toIdentifier() ),
                SADD_ADDED
            );
    }


    /** {@inheritDoc} */
    @Override
    public void removeStreamQueueAndReleaseLock( final DataStream stream ){
        executorService.execute( ()-> {
                hashOperations.delete( streamQueueUrlByIdentifier, stream.toIdentifier() );
                setOperations.remove( streamQueueCreationMutexSet, stream.toIdentifier() );
            });
    }


}
