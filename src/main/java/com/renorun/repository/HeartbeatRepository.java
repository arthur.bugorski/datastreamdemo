package com.renorun.repository;

import com.renorun.service.DataStream;

import java.util.Set;

/**
 * This service is used to maintain a heartbeat so that we can monitor which resources which we maintain but do not
 * control such as Amazon assets (eg SQS topic). This allows us to monitor what may no longer be used so that we can
 * clean up if the user has not reported using it within a certain period of time.
 *
 * The name is a reference to a heartbeat monitor, and when we don't hear from the user for a while (a heartbeat) then
 * we consider it to have flatlined and it to be dead.
 */
public interface HeartbeatRepository {

    /**
     * Start tracking heartbeats for this stream.
     */
    void startTrackingHeartbeatsForStream(DataStream streams);

    /**
     * Update the timestamp of this heartbeat. This should be called after {@link #startTrackingHeartbeatsForStream(DataStream)}
     * and not after {@link #stopTrackingHeartbeatsForStream(DataStream)} and this should prevent the <var>stream</var>
     * from being listed by {@link #listColdHeartbeatStreams()}
     */
    void updateHeartbeat(DataStream stream);

    /**
     * Stops tracking heartbeats for this stream.
     */
    void stopTrackingHeartbeatsForStream(DataStream streams);

    /**
     * This returns the streams that have not called {@link #updateHeartbeat(DataStream)} in a long enough time period so
     * that the resource is considered dead.
     *
     * @return TODO this would be nice to be a reactive stream so that we can use limit and not black everything
     */
    Set<DataStream> listColdHeartbeatStreams();

}
