package com.renorun.repository;

import com.renorun.domain.DataStreamSubscriptionQueue;
import com.renorun.service.DataStream;

import java.util.Optional;

/**
 * This repository stores the queues and subscriptions used by every stream.
 */
public interface StreamQueueDataRepository {

    /**
     * Finds the queue that was stored for that stream or on {@link Optional#empty()} if there is none.
     *
     * This does not require to have acquired the lock to be called.
     */
    Optional< DataStreamSubscriptionQueue > getQueueForStream( DataStream stream );


    /**
     * Sets the queue for the given stream, but it should not be called unless the caller has acquired the lock from 
     * {@link #acquireLockForStreamQueueCreation(DataStream)}
     */
    void setQueueForStream( DataStream username, DataStreamSubscriptionQueue dataStreamSubscriptionQueue );


    /**
     * Try to acquire the lock that indicates that no other instance has created resources for this stream. This is
     * important within scenarios like an impatient refreshing a page whose requests being load balanced across multiple
     * deployed instances of this webservice. Then this allows us te ensure that there is only one instance of the
     * resources acquired.
     *
     * Lock is released with {@link #removeStreamQueueAndReleaseLock}.
     *
     * @return <code>true</code> if the lock was acquired, <code>false</code> otherwise.
     */
    boolean acquireLockForStreamQueueCreation( DataStream stream );


    /**
     * Releases the lock acquired with {@link #acquireLockForStreamQueueCreation(DataStream)} and the data set by
     * {@link #setQueueForStream(DataStream, DataStreamSubscriptionQueue)}.
     */
    void removeStreamQueueAndReleaseLock( DataStream username );

}
