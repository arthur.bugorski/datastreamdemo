package com.renorun.service;

/**
 * This is used to represent an identifier than will be used to identify and thus access a stream. Because not all
 * stream will be for a specific user, this allows us to decouple that. Implementing classes can construct an identifier
 * however they want to, as long as they give us a usable identifier to assign streams for.
 */
public interface DataStream {

    /**
     * This is the token which is used to separate aspects of an identifier as it's composed of constituent parts.
     */
    String SEPARATOR = "_";

    /**
     * Return the identifier that is supposed to be used for this stream.
     */
    String toIdentifier();

}
