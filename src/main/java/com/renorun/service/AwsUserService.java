package com.renorun.service;

import com.renorun.domain.AwsUser;

public interface AwsUserService {

    AwsUser createUser( String userName );
    void delete( String userName );

}
