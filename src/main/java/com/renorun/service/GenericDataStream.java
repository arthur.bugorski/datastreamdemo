package com.renorun.service;

import com.renorun.repository.HeartbeatRepositoryImpl;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Use this when we don't know the source so there are no helpful methods to operate on, but we do know that a stream
 * created by our system does exist. This is currently being used by {@link HeartbeatRepositoryImpl#listColdHeartbeatStreams()}.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GenericDataStream implements com.renorun.service.DataStream {


    private String identifier;


    /** {@inheritDoc} */
    @Override
    public String toIdentifier() {
        return getIdentifier();
    }

}
