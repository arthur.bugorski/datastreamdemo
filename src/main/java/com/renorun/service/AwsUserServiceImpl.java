package com.renorun.service;

import com.renorun.domain.AwsUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import software.amazon.awssdk.core.waiters.WaiterResponse;
import software.amazon.awssdk.services.iam.IamClient;
import software.amazon.awssdk.services.iam.model.*;
import software.amazon.awssdk.services.iam.waiters.IamWaiter;

import java.util.UUID;

@Repository
public class AwsUserServiceImpl implements AwsUserService {

    private final IamClient iamClient;


    @Autowired
    public AwsUserServiceImpl( final IamClient iamClient ){
        this.iamClient = iamClient;
    }


    @Override
    public AwsUser createUser( final String userName ){
        // adapted from:
        //      https://docs.aws.amazon.com/sdk-for-net/v3/developer-guide/iam-users-create.html
        //      https://github.com/awsdocs/aws-doc-sdk-examples/blob/master/javav2/example_code/iam/src/main/java/com/example/iam/CreateUser.java
        try {
            // create user
            final CreateUserRequest request = CreateUserRequest.builder()
                    .userName( userName )
                    .build();

            // Create an IamWaiter object
            IamWaiter iamWaiter = iamClient.waiter();
            final CreateUserResponse createUserResponse = iamClient.createUser(request);

            // Wait until the user is created
            GetUserRequest userRequest = GetUserRequest.builder()
                    .userName(createUserResponse.user().userName())
                    .build();
            final WaiterResponse<GetUserResponse> waitUntilUserExists = iamWaiter.waitUntilUserExists(userRequest);

            if( waitUntilUserExists.matched().exception().isPresent() ){
                // this is Throwable so that is why the catch is for Throwable
                throw waitUntilUserExists.matched().exception().get();
            }
            final User user = createUserResponse.user();

            // set the password
            final String password = createPassword();
            final UpdateLoginProfileRequest updateLoginProfileRequest = UpdateLoginProfileRequest.builder()
                    .userName( user.userName() )
                    .password( password )
                    .build();
            iamClient.updateLoginProfile( updateLoginProfileRequest );

            return AwsUser.builder()
                    .userName( user.userName() )
                    .arn( user.arn() )
                    .password( password )
                    .build();

        }catch( Throwable e ){
            // Throwable because of software.amazon.awssdk.core.internal.waiters.ResponseOrException#exception
            throw new RuntimeException( "Failed to create stream for '" +  userName + "'", e );
        }
    }


    @Override
    public void delete( final String userName ){
        iamClient.deleteUser(
                DeleteUserRequest.builder()
                        .userName( userName )
                        .build()
        );
    }


    private String createPassword(){
        return UUID.randomUUID().toString();
    }

}

