package com.renorun.service;

import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class MessagePublishingServiceImpl implements MessagePublishingService {
    private static final Logger LOGGER = LoggerFactory.getLogger( MessagePublishingServiceImpl.class );

    private static final JsonNodeFactory JSON_NODE_FACTORY = new JsonNodeFactory(false);

    private final AmazonSNSClient amazonSNSClient;
    private final String topicArn;


    @Autowired
    public MessagePublishingServiceImpl(
            final AmazonSNSClient amazonSNSClient,

            @Value( "${datastream.topic.arn}" )
            final String topicArn
    ){
        this.amazonSNSClient = amazonSNSClient;
        this.topicArn = topicArn;
    }


    @Override
    public void updateStreams( final Map< ?, List< String > > messageMetaData, final String message ){
        final Map< String, MessageAttributeValue > messageAttributes = buildMessageMetadata( messageMetaData );

        amazonSNSClient.publish(
                new PublishRequest()
                        .withTopicArn(topicArn)
                        .withMessage(message)
                        .withMessageAttributes(messageAttributes)
        );
        LOGGER.debug( "Sent message {}\n with attributes: {}", message, messageMetaData );
    }


    private Map< String, MessageAttributeValue > buildMessageMetadata( Map< ?, List< String > > messageMetaData ){
        return messageMetaData.entrySet().stream()
            .map(  me -> {
                final Object k = me.getKey();
                final List< String > v = me.getValue();

                final ArrayNode attributeArray = JSON_NODE_FACTORY.arrayNode( v.size() );
                v.forEach( attributeArray::add );

                final MessageAttributeValue attributeValue = new MessageAttributeValue()
                        .withDataType("String.Array")
                        .withStringValue( attributeArray.toString() );

                return Pair.of( k.toString(), attributeValue );
            })
            .collect( Collectors.toMap( Pair::getKey, Pair::getValue ) )
            ;
    }

}
