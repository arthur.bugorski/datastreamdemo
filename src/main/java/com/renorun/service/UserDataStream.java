package com.renorun.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDataStream implements DataStream {

    String username;
    String deviceId;

    @Override
    public String toIdentifier(){
        return username + SEPARATOR + deviceId;
    }

}
