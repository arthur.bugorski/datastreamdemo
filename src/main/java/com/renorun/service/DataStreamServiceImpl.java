package com.renorun.service;

import com.amazonaws.auth.policy.Policy;
import com.amazonaws.auth.policy.Principal;
import com.amazonaws.auth.policy.Resource;
import com.amazonaws.auth.policy.Statement;
import com.amazonaws.auth.policy.actions.SQSActions;
import com.amazonaws.auth.policy.conditions.ArnCondition;
import com.amazonaws.auth.policy.conditions.ConditionFactory;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.SetSubscriptionAttributesRequest;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.amazonaws.services.sns.model.SubscribeResult;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.*;
import com.renorun.domain.AwsUser;
import com.renorun.domain.DataStreamSubscriptionQueue;
import com.renorun.repository.HeartbeatRepository;
import com.renorun.repository.StreamQueueDataRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutorService;

@Service
public class DataStreamServiceImpl implements DataStreamService {
    private static final Logger LOGGER = LoggerFactory.getLogger( DataStreamServiceImpl.class );

    private final AmazonSNSClient amazonSNSClient;
    private final AmazonSQSClient amazonSQSClient;
    private final StreamQueueDataRepository streamQueueDataRepository;
    private final HeartbeatRepository heartbeatRepository;
    private final AwsUserService awsUserService;
    private final ExecutorService executorService;

    private final String topicArn;
    private final Duration maxTimeAway;
    private final String applicationTagValue;
    private final String environmentTagValue;
    private final Duration lockRetryWait;

    @Autowired
    public DataStreamServiceImpl(
            final AmazonSNSClient amazonSNSClient,
            @Value("${datastream.topic.arn}")
            final String topicArn,
            final AmazonSQSClient amazonSQSClient,
            @Value("${datastream.max-time-away-min}")
            final Integer maxTimeAwayMinutes,
            @Value("${datastream.queue.taq.application}")
            final String applicationTagValue,
            @Value("${datastream.queue.taq.environment}")
            final String environmentTagValue,
            final StreamQueueDataRepository streamQueueDataRepository,
            @Value("${datastream.lock.retry-wait-sec}" )
            final int lockRetryWaitSecs,
            final AwsUserService awsUserRepository,
            final HeartbeatRepository heartbeatRepository,
            final ExecutorService executorService
    ){
        this.amazonSNSClient = amazonSNSClient;
        this.topicArn = topicArn;
        this.amazonSQSClient = amazonSQSClient;
        this.maxTimeAway = Duration.ofMinutes(maxTimeAwayMinutes);
        this.streamQueueDataRepository = streamQueueDataRepository;
        this.heartbeatRepository = heartbeatRepository;
        this.awsUserService = awsUserRepository;
        this.executorService = executorService;
        this.applicationTagValue = applicationTagValue;
        this.environmentTagValue = environmentTagValue;
        this.lockRetryWait = Duration.ofSeconds( lockRetryWaitSecs );
    }


    private String getTopicForStream( @SuppressWarnings("unused") final DataStream stream ){
        /*
         * This is hardcoded for now, but you can imagine that this could be tied to a user's market etc.
         */
        return topicArn;
    }


    private DataStreamSubscriptionQueue getOrCreateQueue( final DataStream stream ){

        DataStreamSubscriptionQueue dataStreamSubscriptionQueue = null;
        do {
            final Optional< DataStreamSubscriptionQueue > oUserQueueUrl = streamQueueDataRepository.getQueueForStream( stream );

            if( oUserQueueUrl.isPresent() ){
                // there is an existing stream for the user
                dataStreamSubscriptionQueue = oUserQueueUrl.get();
                executorService.execute(  ()  ->  heartbeatRepository.updateHeartbeat( stream )  );
                LOGGER.info( "Found existing stream for: " + stream );

            }else{
                // if there is no data for the user, then we need to create a connection... but be sure we are the only one
                // doing so to prevent a resource leak, so we have to acquire a lock for the user
                if(  streamQueueDataRepository.acquireLockForStreamQueueCreation( stream )  ){
                    final AwsUser awsUser = awsUserService.createUser( stream.toIdentifier() );

                    //since we have the lock we can create resources for the user
                    final URL queueUrl = createQueueForUser( stream );
                    //  NB: might be able to combine the following two policies into a single call
                    openQueueToTopic( stream, queueUrl );
                    //NB: currently this is overwriting the access to
//                    openQueueToUser( stream, queueUrl, awsUser );
                    final DataStreamSubscriptionQueue dssq = DataStreamSubscriptionQueue.builder()
                            .queue( queueUrl )
                            .awsUser( awsUser )
                            .build()
                            ;
                    dataStreamSubscriptionQueue = dssq;

                    executorService.execute(  ()  ->  heartbeatRepository.startTrackingHeartbeatsForStream( stream )  );
                    LOGGER.debug( "Created new stream for: {}, {}", stream, dssq );

                }else{
                    // we *didn't* get the lock, someone else is creating the credentials... wait and ask for them again
                    try{
                        LOGGER.trace( "Waiting for a lock or data for: {}", stream );
                        Thread.sleep(lockRetryWait.toMillis());
                    }catch ( InterruptedException e ){
                        //TODO log this, maybe count times we have been waiting, maybe max attempt?
                    }
                }
            }


        }while( dataStreamSubscriptionQueue == null );

        return dataStreamSubscriptionQueue;
    }


    /** {@inheritDoc} */
    @Override
    public void keepAlive( final DataStream stream ){
        executorService.execute( () -> heartbeatRepository.updateHeartbeat( stream ) );
    }


    /**
     * This method is not meant to be invoked directly, but rather through some reöccurring scheduled process like the
     * Spring or Quartz. For this reason it is not included in the interface but just in the implementation.
     *
     * This method finds the streams that are inactive (as indicated by their corresponding heartbeat endpoint not being
     * called) and them closes them so that their resources can be reclaimed.
     *
     * NB: this will not fire if {@link org.springframework.scheduling.annotation.EnableScheduling} is not present in the
     * configuration picking up this bean.
     */
    @Scheduled( fixedDelayString = "#{ ${datastream.dead-check-frequency-min} * 60 * 1000 }" )
    public void closeDeadStreams(){
        LOGGER.info( "looking for cold streams" );
        heartbeatRepository.listColdHeartbeatStreams().forEach( stream -> {
                executorService.execute( () -> this.delete( stream ) );
                LOGGER.trace( "Deleted {} for inactivity", stream );
            }
        );
    }


    private void openQueueToTopic( final DataStream stream, final URL queueUrl){
        // https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/java-dg-access-control.html#sqs-example
        final Policy allowTopicToWritePolicy = new Policy().withStatements(
                new Statement(Statement.Effect.Allow)
                        //TODO instead of all users it should just be the SNS Topic that *write* to it
                        .withPrincipals(Principal.AllUsers)
                        .withActions(SQSActions.SendMessage)
                        .withResources(
                                new Resource( toArn(queueUrl) )
                        )
                        .withConditions(
                                new ArnCondition(
                                        ArnCondition.ArnComparisonType.ArnEquals,
                                        ConditionFactory.SOURCE_ARN_CONDITION_KEY,
                                        getTopicForStream(stream)
                                )
                        )
        );
        final String allowTopicToWritePolicyJson = allowTopicToWritePolicy.toJson();

        amazonSQSClient.setQueueAttributes(
                new SetQueueAttributesRequest()
                        .withQueueUrl(queueUrl.toExternalForm())
                        .addAttributesEntry( QueueAttributeName.MessageRetentionPeriod.toString(), Long.toString( maxTimeAway.toSeconds() ) )
                        .addAttributesEntry( QueueAttributeName.Policy.toString(), allowTopicToWritePolicyJson )
        );
    }


    private void openQueueToUser( final DataStream stream, final URL queueUrl, final AwsUser awsUser ){
        final Policy allowUserToReadPolicy = new Policy().withStatements(
                new Statement(Statement.Effect.Allow)
                        //TODO figure out how to tie Principal to the AwsUser
                        .withPrincipals(Principal.AllUsers)
                        .withActions(SQSActions.ReceiveMessage)
                        .withResources(
                                new Resource( toArn(queueUrl) )
                        )
                        .withConditions(
                                new ArnCondition(
                                        ArnCondition.ArnComparisonType.ArnEquals,
                                        ConditionFactory.SOURCE_ARN_CONDITION_KEY,
                                        getTopicForStream(stream)
                                )
                        )
        );
        final String allowTopicToWritePolicyJson = allowUserToReadPolicy.toJson();

        amazonSQSClient.setQueueAttributes(
                new SetQueueAttributesRequest()
                        .withQueueUrl(queueUrl.toExternalForm())
                        .addAttributesEntry( QueueAttributeName.MessageRetentionPeriod.toString(), Long.toString( maxTimeAway.toSeconds() ) )
                        .addAttributesEntry( QueueAttributeName.Policy.toString(), allowTopicToWritePolicyJson )
        );
    }


    /** {@inheritDoc} */
    @Override
    public DataStreamSubscriptionQueue create( final DataStream stream, final Map<Object, List<String>> subscriptionMetadata ){
        final DataStreamSubscriptionQueue queueForStream = getOrCreateQueue( stream );

        if( queueForStream.getSubscriptionArn() == null ){
            LOGGER.debug( "Creating new subscription for: {}", stream );
            final SubscribeResult subscribeRequest = amazonSNSClient.subscribe(
                    new SubscribeRequest()
                            .withTopicArn(getTopicForStream(stream))
                            .withProtocol("sqs")
                            .withEndpoint(toArn(queueForStream.getQueue()))
            );
            queueForStream.setSubscriptionArn( subscribeRequest.getSubscriptionArn() );
            setSubscriptionMetadata( queueForStream, subscriptionMetadata );
            executorService.execute(  ()  ->  streamQueueDataRepository.setQueueForStream( stream, queueForStream )  );
        }

        return queueForStream;
    }


    /** {@inheritDoc} */
    @Override
    public void updateSubscriptionMetadata( final DataStream stream, final Map<?, List<String>> subscriptionData ){
        final DataStreamSubscriptionQueue queueForStream = streamQueueDataRepository.getQueueForStream( stream )
                .orElseThrow( () -> new IllegalStateException( "No queue for: " + stream ) );

        executorService.execute( () -> setSubscriptionMetadata( queueForStream, subscriptionData ) );
    }


    private void setSubscriptionMetadata( final DataStreamSubscriptionQueue queueForStream, final Map<?, List<String>> subscriptionData  ){

        final SNSMessageFilterPolicy fp = new SNSMessageFilterPolicy();
        subscriptionData.forEach( (k,v) -> fp.addAttribute( k.toString(), v ) );

        final SetSubscriptionAttributesRequest request =
                new SetSubscriptionAttributesRequest(
                        queueForStream.getSubscriptionArn(),
                        "FilterPolicy",
                        fp.formatFilterPolicy()
                );
        amazonSNSClient.setSubscriptionAttributes(request);
    }


    /** {@inheritDoc} */
    @Override
    public void delete( final DataStream stream ){
        executorService.execute( () -> {
                final DataStreamSubscriptionQueue dssq = streamQueueDataRepository.getQueueForStream(stream)
                        .orElseThrow(() -> new IllegalStateException("No queue with identifier: " + stream));

                amazonSNSClient.unsubscribe(dssq.getSubscriptionArn());
                amazonSQSClient.deleteQueue(
                        new DeleteQueueRequest()
                                .withQueueUrl(dssq.getQueue().toExternalForm())
                );
                awsUserService.delete( dssq.getAwsUser().getUserName() );

                streamQueueDataRepository.removeStreamQueueAndReleaseLock(stream);
                heartbeatRepository.stopTrackingHeartbeatsForStream(stream);
                LOGGER.debug( "Deleted stream: {}", stream );
            });
    }


    private URL createQueueForUser( final DataStream stream ){
        final String queueName = buildQueueNameForStream( stream );
        try {

            final CreateQueueResult createQueueResult = amazonSQSClient.createQueue(
                    new CreateQueueRequest()
                            .withQueueName(queueName)
                            .withTags(
                                Map.of(
                                        // these tags are both for tracking/auditing purposes both in terms of the resource itself
                                        // and for billing/cost tracing etc
                                        "application", applicationTagValue,
                                        "environment", environmentTagValue
                                  )
                            )
            );
            final String queueUrlStr = createQueueResult.getQueueUrl();
            final URL queueUrl = new URL( queueUrlStr );

            return queueUrl;

        } catch ( QueueNameExistsException | QueueDeletedRecentlyException | MalformedURLException e) {
            //this will never happen because of UUID's uniqueness
            throw new RuntimeException( "Failed to create queue for: " + stream, e );
        }
    }


    private String buildQueueNameForStream( final DataStream stream ){
        // by having a UUID in the name we have uniqueness and prevent collisions, also it increases
        // resilience to hackability since the name aren't sequential nor guessable. By including the
        // username we have some semblance of traceability.
        final UUID userUuid = UUID.randomUUID();

        // by having the current time in the queue name we can then trace back as to when the queue was created so that
        // in the future we can delete queues that are older than 5 days or something if there was a glitch that prevented
        // them from closing off regularly, also adds a bit more entropy to the queue name
        final String currentTime = Long.toString( System.currentTimeMillis() );

        // this creation of this format is what couples this to GenericDataStream
        final String queueName = StringUtils.join(
                new String[]{
                        stream.toIdentifier(),
                        currentTime,
                        userUuid.toString()
                },
                DataStream.SEPARATOR
        );

        return queueName;
    }


    private static String toArn( final URL aUrl ){
        // https://sqs.ca-central-1.amazonaws.com/518380161565/RRTestSqs
        // arn:aws:sqs:ca-central-1:518380161565:RRTestSqs

        final String [] subdomains = aUrl.getHost().split("\\.");
        final String [] folders = aUrl.getPath().split("/");
        return "arn:aws:" + subdomains[0] + ":" + subdomains[1] + ":" + folders[1] + ":" + folders[2];
    }


}
