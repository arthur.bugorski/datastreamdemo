package com.renorun.service;

import java.util.List;
import java.util.Map;

public interface MessagePublishingService {
    void updateStreams( Map< ?, List< String > > messageMetaData, String message );
}
