package com.renorun.service;

import com.renorun.domain.DataStreamSubscriptionQueue;

import java.util.List;
import java.util.Map;

/**
 * This service coördinates lower-level (ie more tied to the implementation and the infrastructure) to provision and
 * reclaim data streams and manages their life-cycle.
 */
public interface DataStreamService {

    /**
     * Takes note that the given <var>stream</var> is still being read from and that it should not be deleted or cleaned
     * up by any automated processes.
     */
    void keepAlive( DataStream stream );

    /**
     * This creates a new datastream (or returns an existing one if it already exists) with the given subscription data.
     */
    DataStreamSubscriptionQueue create( DataStream stream, Map<Object, List<String>> initialSubscriptionData );

    /**
     * Updates the given subscription data for the given stream with the newly provided one.
     */
    void updateSubscriptionMetadata( DataStream stream, Map<?, List<String>> subscriptionData );

    /**
     * Indicates that the stream associated with this is not being used and is no longer needed so that it's associated
     * resources should be cleaned up.
     */
    void delete( DataStream stream );

}
