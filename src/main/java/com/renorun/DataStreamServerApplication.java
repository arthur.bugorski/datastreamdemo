package com.renorun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//see: https://stackoverflow.com/a/66418405/254477
@SpringBootApplication
(
        exclude = {
                org.springframework.cloud.aws.autoconfigure.context.ContextInstanceDataAutoConfiguration.class,
                org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration.class,
                org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration.class
        }
)
public class DataStreamServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataStreamServerApplication.class, args);
    }

}
