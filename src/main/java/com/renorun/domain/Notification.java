package com.renorun.domain;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class Notification {
    private Map< String, List<String> > metadata;
    private String message;
}
