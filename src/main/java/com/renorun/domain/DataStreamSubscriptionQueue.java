package com.renorun.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataStreamSubscriptionQueue {

    private static final String QUEUE_SUBSCRIPTION_SEPARATOR = "|";
    private static final String QUEUE_SUBSCRIPTION_SEPARATOR_REGEX = "\\" + QUEUE_SUBSCRIPTION_SEPARATOR;

    private enum Field{
        QUEUE_URL,
        SUBSCRIPTION_ARN,
        AWS_USER_NAME,
        AWS_USER_ARN,
        AWS_USER_PASSWORD
    }

    URL queue;
    String subscriptionArn;
    Map< ?, List<String> > subscriptionMetadata;

    AwsUser awsUser;


    /*
     * These methods are both static so this con bo moved into a dedicated SerDe (SERializer/DEserializer) object in the future.
     *
     * This is basically packing all the relevant bits into a single String to make it getting pulled from Redis in a
     * single call possible. Not using JSON since this is faster and there is no need to go that heavy but you can easily
     * do that with this encapsulation.
     *
     * Another Redis-friendly way to do this would be to use separate keys and then just use EVAL and send a Lua script
     * to pack all the bits together. That may be an option in the future.
     *
     * Possibly could have just used Java serialisation and saved the byte String and just pulled that but this is more
     * serialisable and serialization may get deprecated as there has been talk to that effect.
     */

    public static DataStreamSubscriptionQueue deserialise( final String serialisedDataStreamSubscriber ){

        // this whole mess is needed to handle an empty string being contained by two delimiters
         final StringTokenizer stringTokenizer = new StringTokenizer( serialisedDataStreamSubscriber, QUEUE_SUBSCRIPTION_SEPARATOR, true );

        final String[] serialisedDataStreamFieldValues = new String[ Field.values().length ];
        for( int i = 0; stringTokenizer.hasMoreTokens(); i++ ){
            final String token = stringTokenizer.nextToken();

            if( token.equals( QUEUE_SUBSCRIPTION_SEPARATOR ) ){
                serialisedDataStreamFieldValues[ i ] = "";

            }else{
                serialisedDataStreamFieldValues[ i ] = token;
                final String consumeDelimeter = stringTokenizer.nextToken();
                assert consumeDelimeter.equals( QUEUE_SUBSCRIPTION_SEPARATOR );
            }
        }

        final URL queueUrl;
        try{
            final String queueUrlString = serialisedDataStreamFieldValues[ Field.QUEUE_URL.ordinal() ];
            queueUrl = new URL( queueUrlString );

        }catch( MalformedURLException e ){
            throw new IllegalArgumentException(e);

        }

        return DataStreamSubscriptionQueue.builder()
                .queue( queueUrl )
                .subscriptionArn         ( serialisedDataStreamFieldValues[ Field.SUBSCRIPTION_ARN  .ordinal() ] )
                .awsUser(
                        AwsUser.builder()
                                .userName( serialisedDataStreamFieldValues[ Field.AWS_USER_NAME     .ordinal() ] )
                                .arn     ( serialisedDataStreamFieldValues[ Field.AWS_USER_ARN      .ordinal() ] )
                                .password( serialisedDataStreamFieldValues[ Field.AWS_USER_PASSWORD .ordinal() ] )
                                .build()
                )
                .build();
    }


    public static String serialise( final DataStreamSubscriptionQueue dssq ){
        final Map< Field, String > fieldValuesByField = Map.of(
                Field.QUEUE_URL,         dssq.queue.toExternalForm(),
                Field.SUBSCRIPTION_ARN,  dssq.subscriptionArn == null ? "" : dssq.subscriptionArn,
                Field.AWS_USER_NAME,     dssq.awsUser == null ? "" : dssq.awsUser.getUserName(),
                Field.AWS_USER_ARN,      dssq.awsUser == null ? "" : dssq.awsUser.getArn(),
                Field.AWS_USER_PASSWORD, dssq.awsUser == null ? "" : dssq.awsUser.getPassword()
        );

    return Stream.of( Field.values() )
            .map( fieldValuesByField::get )
            .map( o -> ObjectUtils.defaultIfNull( o, "" ) )
            .collect( Collectors.joining( QUEUE_SUBSCRIPTION_SEPARATOR, "", QUEUE_SUBSCRIPTION_SEPARATOR ) );
    }

}
